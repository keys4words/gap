# gap
play with gap - stack

## Infra

2 VM:
- 192.168.50.129 - wordpress + php-fpm8.2 + mysql + nginx
- 192.168.50.22 - prometheus + alertmanager + grafana + victoriametrics

## Monitoring
### infra
- node status - node_exporters for both nodes
### components
- mysql - mysql_exporter
- php-fpm - php-fmp_exporter
- nginx - nginx_exporter
### app
- ping main page - blackbox_exporter
### monitoring infa
- prometheus
- alertmanager
- victoriametrics

## Prometheus rules
- add 1 custom rule for monitoring free disk memory with severity - warning
- add 1 custom rule for monitoring instance down with severity - critical

## Alerts (alertmanager)
- email receiver - when any rule with severity level = warning
- telegram receiver - when any rule with severity level = critical

## Alerts (grafana)
- telegram telepush - play with severity level

# Aggregation Grafana's dashboard
- App dashboard - screenshot
- Infra dashboard - screenshot